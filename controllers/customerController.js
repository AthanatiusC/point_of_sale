let db = require('../config/connection')

exports.index = function(req, res){
    db.query("SELECT * FROM customers", function(err,result){
    	if(err) res.status(500).send({'result':err})

    		res.status(200).send({'result':result})
    })
}
exports.detail = function(req, res){
    let id = req.params.id
    db.query("SELECT * FROM customers WHERE id=?",[id], function(err,result){
    	if(err) res.status(500).send({'result':err})

    		res.status(200).send({'result':result})
    })
}
exports.store = function(req, res){
    let data = {
    	name : req.body.name,
    	email : req.body.email,
    	phone : req.body.phone,
    	address : req.body.address,
    }
    db.query('INSERT INTO customers (name, email, phone, address) VALUES (?,?,?,?)',
    	[data.name, data.email, data.phone, data.address], function(error,results){
    	if (error) throw error;
    	let id = results.insertId;
    	if (id) {
    		data.id == results.insertId;
    		res.send(data,201)
    	}else{
    		res.send(400)
    	}
    })
}
exports.update = function(req, res){
    let id = req.params.id
    let data = {
        name : req.body.name,
    	email : req.body.email,
    	phone : req.body.phone,
    	address : req.body.address,
    }
    db.query('UPDATE customers set name=?, email=?, phone=?, address=? WHERE id=?',
        [data.name, data.email, data.phone, data.address, id],
        function (error, results) {
            if (error) throw error
            let changedRows = results.changedRows;
            if (changedRows > 0) {
                data.id = id
                res.send(data, 200)
            } else {
                res.send(404)
            }
        })
}
exports.delete = function(req, res){
    var id = req.params.id
    db.query('DELETE FROM customers WHERE id=?' ,[id], function(error,result){
    	if (error) throw error;

    	if(result.affectedRows > 0){
    		res.send(204)
    	}else{
    		res.send(404)
    	}
    })
}