let db = require("../config/connection");

exports.index = function(req, res) {
  db.query("SELECT * FROM orders", function(err, result) {
    if (err)
      res.status(500).send({
        result: err
      });

    res.status(200).send({
      result: result
    });
  });
};

exports.detail = function(req, res) {
  let params_id = req.params.id;
  db.query("SELECT * FROM orders WHERE id = ?", params_id, function(
    err,
    result
  ) {
    if (err)
      res.status(500).send({
        result: err
      });

    res.status(200).send({
      result: result
    });
  });
};

exports.store = function(req, res) {
  let data = {
    user_id: req.body.user_id,
    customer_id: req.body.customer_id,
    payment_method_id: req.body.payment_method_id,
    total: req.body.total,
    bayar: req.body.bayar,
    kembalian: req.body.kembalian
  };
  db.query(
    "INSERT INTO orders (user_id, customer_id, payment_method_id, total, bayar, kembalian) VALUES (?,?,?,?,?,?)",
    [
      data.user_id,
      data.customer_id,
      data.payment_method_id,
      data.total,
      data.bayar,
      data.kembalian
    ],
    function(error, results) {
      if (error) throw error;

      let id = results.insertId;
      if (id) {
        data.id = results.insertId;
        res.send(data, 201);
      } else {
        res.send(400);
      }
    }
  );
};

exports.update = function(req, res) {
  let id = req.params.id;
  let data = {
    user_id: req.body.user_id,
    customer_id: req.body.customer_id,
    payment_method_id: req.body.payment_method_id,
    total: req.body.total,
    bayar: req.body.bayar,
    kembalian: req.body.kembalian
  };
  db.query(
    "UPDATE orders set user_id=?, customer_id=?, payment_method_id=?, total=?, bayar=?, kembalian=? WHERE id=?",
    [
      data.user_id,
      data.customer_id,
      data.payment_method_id,
      data.total,
      data.bayar,
      data.kembalian,
      id
    ],
    function(error, results) {
      if (error) throw error;
      let changedRows = results.changedRows;
      if (changedRows > 0) {
        data.id = id;
        res.send(data, 200);
      } else {
        res.send(404);
      }
    }
  );
};

exports.delete = function(req, res) {
  var id = req.params.id;
  db.query("DELETE FROM orders WHERE id = ?", [id], function(error, results) {
    if (error) throw error;

    if (results.affectedRows > 0) {
      res.send(204);
    } else {
      res.send(404);
    }
  });
};