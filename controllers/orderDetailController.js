let db = require('../config/connection')
 
exports.index = function (req, res) {
    db.query("SELECT * FROM order_details", function (err, result) {
        if (err) res.status(500).send({
            'result': err
        })
 
        res.status(200).send({
            'result': result
        })
    })
}
 
exports.detail = function (req, res) {
    let params_id = req.params.id
    db.query("SELECT * FROM order_details WHERE id = ?", params_id, function (err, result) {
        if (err) res.status(500).send({
            'result': err
        })
 
        res.status(200).send({
            'result': result
        })
    })
}
 
exports.store = function (req, res) {
    let data = {
        order_id: req.body.order_id,
        product_id: req.body.product_id,
        quantity: req.body.quantity,
        price: req.body.price,
        sub_total: req.body.sub_total,
    }
    db.query('INSERT INTO order_details (order_id, product_id, quantity, price, sub_total) VALUES (?,?,?,?,?)',
        [data.order_id, data.product_id, data.quantity, data.price, data.sub_total],
        function (error, results) {
            if (error) throw error;
 
            let id = results.insertId;
            if (id) {
                data.id = results.insertId;
                res.send(data, 201)
            } else {
                res.send(400)
            }
        })
}
 
exports.update = function (req, res) {
    let id = req.params.id
    let data = {
        order_id: req.body.order_id,
        product_id: req.body.product_id,
        quantity: req.body.quantity,
        price: req.body.price,
        sub_total: req.body.sub_total,
    }
    db.query('UPDATE order_details set order_id=?, product_id=?, quantity=?, price=?, sub_total=? WHERE id=?',
        [data.order_id, data.product_id, data.quantity, data.price, data.sub_total, id],
        function (error, results) {
            if (error) throw error
            let changedRows = results.changedRows;
            if (changedRows > 0) {
                data.id = id
                res.send(data, 200)
            } else {
                res.send(404)
            }
        })
}
 
exports.delete = function (req, res) {
    var id = req.params.id
    db.query('DELETE FROM order_details WHERE id = ?', [id],
        function (error, results) {
            if (error) throw error
 
            if (results.affectedRows > 0) {
                res.send(204)
            } else {
                res.send(404)
            }
        })
}