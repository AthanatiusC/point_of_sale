let db = require('../config/connection')

exports.index = function(req, res){
    db.query("SELECT * FROM payment_methods", function(err,result){
    	if(err) res.status(500).send({'result':err})

    		res.status(200).send({'result':result})
    })
}
exports.detail = function(req, res){
    let id = req.params.id
    db.query("SELECT * FROM payment_methods WHERE id=?", id, function(err, result){
    	if(err) res.status(500).send({'result':err})

    		res.status(200).send({'result':result})
    })
}
exports.store = function(req, res){
    
    let data = {
    	name : req.body.name,
    	description : req.body.description,
    	is_active : req.body.is_active,
    }
    db.query('INSERT INTO payment_methods (name, description, is_active) VALUES (?,?,?)',
    	[data.name, data.description, data.is_active], function(error,results){
    	if (error) throw error;

    	let id = results.insertId;
    	if (id) {
    		data.id == results.insertId;
    		res.send(data,201)
    	}else{
    		res.send(400)
    	}

})
}
exports.update = function(req, res){
    let id = req.params.id
    let data = {
        name : req.body.name,
    	description : req.body.description,
    	is_active : req.body.is_active,
    }
    db.query('UPDATE payment_methods set name=?, description=?, is_active=? WHERE id=?',
        [data.name, data.description, data.is_active, id],
        function (error, results) {
            if (error) throw error
            let changedRows = results.changedRows;
            if (changedRows > 0) {
                data.id = id
                res.send(data, 200)
            } else {
                res.send(404)
            }
        })
}
exports.delete = function(req, res){
    var id = req.params.id
    db.query('DELETE FROM payment_methods WHERE id=?' ,[id], function(error,result){
    	if (error) throw error;

    	if(result.affectedRows > 0){
    		res.send(204)
    	}else{
    		res.send(404)
    	}
    })
} 