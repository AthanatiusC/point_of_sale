let db = require('../config/connection')

exports.index = function (req, res) {
    db.query("SELECT * FROM products", function (err, result) {
        if (err) res.status(500).send({
            'result': err
        })

        res.status(200).send({
            'result': result
        })
    })
}

exports.detail = function (req, res) {
    let params_id = req.params.id
    db.query("SELECT * FROM products WHERE id = ?", params_id, function (err, result) {
        if (err) res.status(500).send({
            'result': err
        })

        res.status(200).send({
            'result': result
        })
    })
}

exports.store = function (req, res) {
    let data = {
        category_id: req.body.category_id,
        code: req.body.code,
        name: req.body.name,
        description: req.body.description,
        stock: req.body.stock,
        price: req.body.price,
        pic_image: req.body.pic_image,
    }
    db.query('INSERT INTO products (category_id, code, name, description, stock, price, pic_image) VALUES (?,?,?,?,?,?,?)',
        [data.category_id, data.code, data.name, data.description, data.stock, data.price, data.pic_image],
        function (error, results) {
            if (error) throw error;

            let id = results.insertId;
            if (id) {
                data.id = results.insertId;
                res.send(data, 201)
            } else {
                res.send(400)
            }
        })
}

exports.update = function (req, res) {
    let id = req.params.id
    let data = {
        category_id: req.body.category_id,
        code: req.body.code,
        name: req.body.name,
        description: req.body.description,
        stock: req.body.stock,
        price: req.body.price,
        pic_image: req.body.pic_image,
    }
    db.query('UPDATE products set category_id=?, code=?, name=?, description=?, stock=?, price=?, pic_image=? WHERE id=?',
        [data.category_id, data.code, data.name, data.description, data.stock, data.price, data.pic_image, id],
        function (error, results) {
            if (error) throw error
            let changedRows = results.changedRows;
            if (changedRows > 0) {
                data.id = id
                res.send(data, 200)
            } else {
                res.send(404)
            }
        })
}

exports.delete = function (req, res) {
    var id = req.params.id
    db.query('DELETE FROM products WHERE id = ?', [id],
        function (error, results) {
            if (error) throw error

            if (results.affectedRows > 0) {
                res.send(204)
            } else {
                res.send(404)
            }
        })
}